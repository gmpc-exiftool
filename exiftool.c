/* gmpc-exiftool (GMPC plugin)
 * Copyright (C) 2006-2010 Qball Cow <qball@sarine.nl>
 * Project homepage: http://gmpcwiki.sarine.nl/
 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include <stdio.h>
#include <string.h>
#include <gtk/gtk.h>
#include <config.h>
#include <glib.h>
#include <gmpc/plugin.h>
#include <gmpc/metadata.h>
#include <libmpd/libmpd.h>
#include <libmpd/debug_printf.h>
#include <config.h>
#include <EXTERN.h>
#include <perl.h>
static PerlInterpreter *my_perl;
int fetch_get_image(mpd_Song *song,MetaDataType type,void (*callback)(GList *uris, gpointer data), gpointer data) ;
void music_dir_cover_art_pref_construct(GtkWidget *container);
void music_dir_cover_art_pref_destroy(GtkWidget *container);
GList * fetch_cover_art_path_list(mpd_Song *song);
GList * fetch_cover_art_path(mpd_Song *song);
static GtkWidget *wp_pref_vbox = NULL;

gmpcPlugin plugin;

/**
 * Required functions
 */
/*
 * Enable/Disable state of the plugins
 */
static int exiftool_get_enabled()
{
	return cfg_get_single_value_as_int_with_default(config, "exiftool-cover", "enable", TRUE);
}
static void exiftool_set_enabled(int enabled)
{
	cfg_set_single_value_as_int(config, "exiftool-cover", "enable", enabled);
}
/* priority */
static int fetch_cover_priority()
{
    return cfg_get_single_value_as_int_with_default(config, "exiftool-cover", "priority", 10);
}

static void fetch_cover_priority_set(int priority)
{
    cfg_set_single_value_as_int(config, "exiftool-cover", "priority", priority);
}



int fetch_get_image(mpd_Song *song,MetaDataType type,void (*callback)(GList *uris, gpointer data), gpointer data) 
{
    gchar *path = NULL;
    MetaData *md = NULL;
    GList *list = NULL;
    int num_image = 0;
    int retval  = META_DATA_UNAVAILABLE;
	if(song  == NULL || song->file == NULL)
	{
        debug_printf(DEBUG_INFO, "ExifTool:  No file path to look at.");
        callback(NULL, data);
		return META_DATA_UNAVAILABLE;
	}
	if(type == META_ALBUM_ART)
	{
        const gchar *musicroot= connection_get_music_directory();
        if(musicroot)
		{
			gchar *file = g_build_filename(musicroot, song->file, NULL); 
			if(g_file_test(	file, G_FILE_TEST_EXISTS))
			{
                gchar *perl_program;
                STRLEN length = 0;
                char *embedding[] = {"", "-e", "0"};
                // run Perl program
                my_perl = perl_alloc();
                perl_construct( my_perl );
                perl_parse(my_perl, NULL, 3,embedding, (char **)NULL);
                PL_exit_flags |= PERL_EXIT_DESTRUCT_END;

                perl_run(my_perl);
                eval_pv("use Image::ExifTool ':Public'", TRUE);
                SV *sv2 = get_sv ("path", TRUE);
                sv_setpv(sv2,file); 
                eval_pv("$info = ImageInfo($path)", TRUE);
                eval_pv("$return = $$info{'Picture'};", TRUE);
                SV *sv = get_sv("return", FALSE);
                while(sv && SvROK(sv) != 0)
                {
                    /* Reference.. */
                    sv = SvRV(sv);
                    gchar *image = SvPV(sv, length);
                    if(length> 0)
                    {
                        printf("Exif data tool got image: %s\n", file);
                        md = meta_data_new();
                        md->type = type;
                        md->plugin_name = plugin.name; 
                        md->content_type = META_DATA_CONTENT_RAW;
                        md->content = g_memdup(image, length);
                        md->size = length;
                        list = g_list_append(list, md);
                        retval = META_DATA_AVAILABLE;
                        num_image++;
                    }
                    path = g_strdup_printf("$return = $$info{'Picture (%i)'};",num_image);
                    eval_pv(path, TRUE);
                    g_free(path);
                    sv = get_sv("return", FALSE);
                }
                perl_destruct(my_perl);
                perl_free(my_perl);
                g_free(perl_program);
			}
			g_free(file);
		}
	}
	
    callback(list, data);
    return retval; 
}



gmpcMetaDataPlugin exiftool_cover = {
	.get_priority = fetch_cover_priority,
    .set_priority = fetch_cover_priority_set,
	.get_metadata = fetch_get_image
};
/* a workaround so gmpc has some sort of api checking */
int plugin_api_version = PLUGIN_API_VERSION;

void exiftool_init(void)
{
}
static const gchar *exiftool_get_translation_domain(void)
{
    return NULL;
}
/* the plugin */
gmpcPlugin plugin = {
	.name           = ("Exiftool Dir Fetcher"),
	.version        = {0,0,1},
	.plugin_type    = GMPC_PLUGIN_META_DATA,
    .init           = exiftool_init,
	.metadata       = &exiftool_cover, /* meta data */
	.get_enabled    = exiftool_get_enabled,
	.set_enabled    = exiftool_set_enabled,
    .get_translation_domain = exiftool_get_translation_domain
};
